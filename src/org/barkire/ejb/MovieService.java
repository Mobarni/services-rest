package org.barkire.ejb;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.barkire.model.Movie;

@Stateless
public class MovieService {


	@PersistenceContext(unitName="jpa-prod")
	private EntityManager em;
	public Movie findById(long id) {
		// TODO Auto-generated method stub
		
		return em.find(Movie.class, id);
	}

}
