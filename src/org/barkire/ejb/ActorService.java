package org.barkire.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.barkire.model.Actor;

@Stateless
public class ActorService {

		@PersistenceContext(unitName="jpa-prod")
		private EntityManager em;
		
		public Actor findById(long id){
			
			return em.find(Actor.class, id);
			
		}
}
