package org.barkire.model;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.barkire.ejb.ActorService;

@Path("actor")
public class ActorRS {

	@EJB
	ActorService actorService= new ActorService();
	
	@Path("id")
	@GET
	public Response findById(@PathParam("id") long id){
		Actor actor= actorService.findById(id);
		if(actor==null){
			return Response.status(Status.NOT_FOUND).build();
		}
		else{
			return Response.ok().entity(actor).build();
		}
		
		
	}
}
