package org.barkire.rest.impl;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.ws.rs.ext.Provider;

import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.core.spi.component.ComponentScope;
import com.sun.jersey.spi.inject.Injectable;
import com.sun.jersey.spi.inject.InjectableProvider;

import java.lang.reflect.Type;



 @Provider
 public  class EJBProvider  implements InjectableProvider<EJB, Type> {

     public ComponentScope getScope() {
         return ComponentScope.Singleton;
    }

     @SuppressWarnings("rawtypes")
	public Injectable getInjectable(ComponentContext context, EJB ejb, Type t) {
      
         if (!(t  instanceof Class)) 
             return null;

         try {
            Class clazz = (Class)t ;
            Context initialContext =  new InitialContext();

            String componentName = clazz.getName() ;
             if (ejb.mappedName() != null) {
                componentName = ejb.mappedName() ;
            }

             final Object ejbInstance = initialContext.lookup(componentName);

             return  new Injectable() {
                 public Object getValue() {
                     return ejbInstance ;
                }
            };
        }  catch (Exception e) {
             return null;
        }
    }
}