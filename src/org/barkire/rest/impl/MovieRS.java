package org.barkire.model;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.barkire.ejb.MovieService;

@Path("movie")
public class MovieRS {

	@EJB
	private MovieService movieService= new MovieService();
	
	@Path("{id}")
	@GET
	public Response findById(@PathParam("id") long id){
		Movie movie= movieService.findById(id);
		if(movie==null){
			return Response.status(Status.NOT_FOUND).build();
		}
		else{
			return Response.ok().entity(movie).build();
		}
	}
}
