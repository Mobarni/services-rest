package org.barkire.rest.hello;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("hello")
public class HelloRest {

	@Path("first/{string}")
	@GET
	public String hello(@PathParam("string") String name){
		return "<h1>Hello form REST!</h1>"+ name;
		
	}
}
