package org.barkire.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
public class Movie {

	@XmlAttribute
	@Id @GeneratedValue(strategy= GenerationType.TABLE)
	private long id;
	
	@XmlElement(name="title")
	private String nom;
	
	@XmlElement(name="release-year")
	private long date;
	
	
	@ManyToMany(cascade=CascadeType.PERSIST)
	@XmlElementWrapper(name="actors")
	@XmlElement(name="actor")
	private Set<Actor> actors;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	

	/**
	 * @return the actors
	 */
	public Set<Actor> getActors() {
		return actors;
	}
	/**
	 * @param actors the actors to set
	 */
	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Movie [id=" + id + ", nom=" + nom + ", date=" + date + ", actors=" + actors + "]";
	}
}
