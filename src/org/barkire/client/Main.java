package org.barkire.client;

import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.barkire.ejb.MovieService;
import org.barkire.model.Movie;

public class Main {

	public static void main(String [] args) throws NamingException{
		// cr�ation du "contexte initial" = de la connexion � l'annuaire du serveur
		Context context = null;
	    String host = "127.0.0.1";
	    String port = "8080";
	    Properties props = new Properties();

	    props.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
	    props.setProperty("java.naming.factory.url.pkgs", "com.sun.enterprise.naming");
	    props.setProperty("java.naming.factory.state", "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
	    props.setProperty("org.omg.CORBA.ORBInitialHost", host);
	    props.setProperty("org.omg.CORBA.ORBInitialPort", port);

	    try
	    {
	        context = new InitialContext(props);
	    }
	    catch (NamingException e)
	    {
	        System.out.println("JMSCommon Error: Can't init JNDI Context" + "(" + e + ")");
	        System.exit(0);
	    }
        
         // requ�te sur le nom de la ressource que l'on veut, ici notre EJB
        MovieService movieService = new MovieService();
		try {
			movieService = (MovieService)context.lookup("MovieService");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
         // invocation d'une m�thode

         Movie movie= movieService.findById(52);

        System.out.println("Film = " + movie) ;
        
        List<Movie> movies = movieService.findAllMovies() ;
        for (Movie m : movies) {
           System.out.println("Film = " + m) ;
       }
        
	}
}
